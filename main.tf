# Terraform configuration goes here
provider "google" {
  project = ""
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_project_services" "project_services" {
  project  = "terraform-enterprise-254816"
  services = ["compute.googleapis.com", "oslogin.googleapis.com"]
}

resource "google_compute_network" "vpc_network" {
  project  = "terraform-enterprise-254816"
  name = "terraform-network"
  depends_on = [google_project_services.project_services]  
}

resource "google_compute_instance" "vm_instance" {
  project  = "terraform-enterprise-254816"    
  name         = "terraform-instance"
  machine_type = "f1-micro"
  tags         = ["web", "dev"]  

  boot_disk {
    initialize_params {
      image = "cos-cloud/cos-stable"
    }
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
    }
  }
}
